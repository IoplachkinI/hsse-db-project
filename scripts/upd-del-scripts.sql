update project.cars
    set color = 'Цвет морской волны'
    where color = 'Морской синий';

update project.staff
    set salary = '150000'
    where last_name = 'Тюленев';

update project.car_models
    set body_type = 'Банан'
    where model_name like 'CLS%';

-- Прощай, легенда
delete from project.staff
       where first_name = 'Райан' and last_name = 'Гослинг';

-- Прощай, Мерседес Банан черного цвета
delete from project.cars
       where model_id in
             (select model_id
              from project.car_models
              where body_type = 'Банан');