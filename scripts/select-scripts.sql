-- Select'ы
select * from project.dealerships where monthly_cost < 10000000;

select model_name, additional_options
    from project.car_models join project.cars on car_models.model_id = cars.model_id
    where cars.additional_options like '%Launch Control%'
    order by model_name;

select color, sum(price) as price_sum
    from project.cars
    group by color
    order by price_sum;

select dealer_name, car_manufacturer_name, monthly_sum
    from project.dealer
    join (
        select dealer_id, sum(monthly_cost) as monthly_sum
        from project.dealerships
        group by dealer_id
        having sum(monthly_cost) > 11000000
        order by dealer_id) as e
    on e.dealer_id = dealer.dealer_id;

select *
    from project.spare_parts
    where part_name like 'Перед%';

-- Оконки
select dealer_id, part_name, rank()
          over (partition by dealer_id order by part_name)
    from project.spare_parts;

select body_type, model_name, power, rank()
          over (partition by body_type order by power) as power_rank
    from project.cars join project.car_models on cars.model_id = car_models.model_id
    order by body_type;

select body_type, model_name, power, torque, avg(torque)
          over (partition by body_type) as avg_torque
    from project.cars join project.car_models on cars.model_id = car_models.model_id
    order by body_type;

with engine_lag as (select *, lag(engine_size, 1, 1) over (order by engine_size) as lag
    from project.cars join project.car_models on cars.model_id = car_models.model_id
    order by engine_size)
select model_name, power, torque, engine_size,
       round(((engine_size - lag) / lag * 100)::numeric, 2) as "diff%"
    from engine_lag;

select dealership_id, first_name, last_name, salary,
       min(salary) over (partition by dealership_id)
           as min_salary from project.staff;
