create or replace view premium_cars as
select concat(car_manufacturer_name, ' ', model_name) as model_name, body_type, power, color, price
from project.cars
    join project.car_models on cars.model_id = car_models.model_id
    join project.dealer on car_models.dealer_id = dealer.dealer_id
where price > 5000000;

select * from premium_cars;

create or replace view low_grade_workers as
    select first_name, last_name, middle_name, occupation, salary
from project.staff
where salary < 50000
with cascaded check option;

select * from low_grade_workers;

create or replace view dealer_stock as
    select dealer_name, concat(car_manufacturer_name, ' ', model_name) as model_name
from project.dealer join project.car_models on dealer.dealer_id = car_models.dealer_id;


select * from dealer_stock;

create or replace view parts as
    select concat(car_manufacturer_name, ' ', model_name) as model_name, part_name
from project.spare_parts
    join project.car_models on spare_parts.model_id = car_models.model_id
    join project.dealer on car_models.dealer_id = dealer.dealer_id;

select * from parts;