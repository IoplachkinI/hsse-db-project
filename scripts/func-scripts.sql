create or replace function get_full_names() returns table(full_name text)
as $$ select concat(car_manufacturer_name, ' ', model_name) from
    project.car_models join project.dealer on car_models.dealer_id = dealer.dealer_id
    $$ language sql;

select * from get_full_names();

create or replace function get_total_salary() returns table(dealer_name text, total_sal int)
as $$ select distinct(dealer_name), sum(salary) over (partition by dealer.dealer_name) as total_sal
    from project.staff
    join project.dealerships on staff.dealership_id = dealerships.dealership_id
    join project.dealer on dealerships.dealer_id = dealer.dealer_id;
    $$ language sql;

select * from get_total_salary();

create or replace function get_cars_cheaper_than(integer)
    returns table(name text, body_type text, power int, torque int, engine_size numeric, price int)
as $$ select concat(car_manufacturer_name, ' ', model_name) as name, body_type, power, torque, engine_size, price
      from project.car_models
    join project.cars on car_models.model_id = cars.model_id
    join project.dealer on car_models.dealer_id = dealer.dealer_id
    where price < $1;
    $$ language sql;

select * from get_cars_cheaper_than(10000000);

create or replace function del_dealership_dependencies() returns trigger
as $$ begin
    delete from project.staff where dealership_id = old.dealership_id;
    delete from project.cars where dealership_id = old.dealership_id;
    return new;
    end
    $$ language plpgsql;

create or replace trigger del_dealership
    before delete on project.dealerships
    for each row
    execute procedure del_dealership_dependencies();

delete from project.dealerships where dealership_id = 8;