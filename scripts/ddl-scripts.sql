create schema project;

set search_path to project;

create table dealer
(
    "dealer_id"             serial not null,
    "dealer_name"           text,
    "car_manufacturer_name" text,
    constraint pk_dealer primary key ("dealer_id")
);

CREATE TABLE dealerships
(
    "dealer_id"     serial not null,
    "dealership_id" serial not null,
    "address"       text,
    "monthly_cost"  integer,
    constraint fk_dealer foreign key ("dealer_id") references dealer ("dealer_id"),
    constraint pk_dealership primary key ("dealership_id"),
    constraint chk_cost check ("monthly_cost" > 0)
);

CREATE TABLE staff
(
    "dealership_id" serial  not null,
    "employee_id"   serial  not null,
    "first_name"    text    not null,
    "last_name"     text    not null,
    "middle_name"   text,
    "occupation"    text,
    "salary"        integer not null,
    constraint fk_dealership foreign key ("dealership_id") references dealerships ("dealership_id"),
    constraint pk_employee primary key ("employee_id"),
    constraint chk_salary check ("salary" > 0)
);

CREATE TABLE car_models
(
    "dealer_id"   serial not null,
    "model_id"    serial not null,
    "model_name"  text   not null,
    "body_type"   text,
    "power"       integer,
    "torque"      integer,
    "engine_size" real,
    constraint fk_dealer foreign key ("dealer_id") references dealer ("dealer_id"),
    constraint pk_model primary key ("model_id"),
    constraint chk_power check ("power" > 0),
    constraint chk_torque check ("torque" > 0),
    constraint chk_engine_size check ("engine_size" > 0)
);

CREATE TABLE cars
(
    "dealership_id"      serial  not null,
    "model_id"           serial  not null,
    "car_id"             serial  not null,
    "color"              text,
    "additional_options" text,
    "price"              integer not null,
    constraint fk_dealership foreign key ("dealership_id") references dealerships ("dealership_id"),
    constraint fk_model foreign key ("model_id") references car_models ("model_id"),
    constraint pk_car primary key ("car_id"),
    constraint chk_price check ("price" > 0)
);

CREATE TABLE spare_parts
(
    "dealer_id" serial not null,
    "model_id"  serial not null,
    "part_id"   serial not null,
    "part_name" text   not null,
    constraint fk_dealer foreign key ("dealer_id") references dealer ("dealer_id"),
    constraint fk_model foreign key ("model_id") references car_models ("model_id"),
    constraint pk_part primary key ("part_id")
);
